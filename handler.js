'use strict';

const fs = require('fs');
const ACS = require('acs-node');
const querystring = require('querystring');
const moment = require('moment-timezone');
const _ = require('underscore');
const tmp = require('tmp');
const archiver = require('archiver');
const async = require("async");
const request = require('request');
const btoa = require('btoa');
const sjcl = require('./src/js/sjcl.js');
const nodemailer = require("nodemailer");
const AWS = require('aws-sdk')

module.exports.routes = (event, context, callback) => {

    const srcPath = './src';
    const path = event.path;
    const fileExtention = path.split('.').pop();
    const fileContents = fs.readFileSync(srcPath + path, 'UTF-8');

    const response = {
        statusCode: 200,
        headers: {
            "Access-Control-Allow-Origin": "*", // Required for CORS support to work
            "Content-Type": getContentType(fileExtention),
        },
        body: fileContents
    };
    callback(null, response);
};

const getContentType = (fileExtention) => {

    let responseContentType = '';

    if (fileExtention === 'html') {
        responseContentType = "text/html";
    } else if (fileExtention === 'css') {
        responseContentType = "text/css";
    } else if (fileExtention === 'js') {
        responseContentType = "application/javascript";
    }
    return responseContentType;
}

// archive function
module.exports.archive = (event, context, callback) => {

    const params = querystring.parse(event.body);
    const login = params['login'];
    const pin = params['pin'];
    const sendTo = params['sendTo'];
    const actionType = params['actionType'];

    ACS.initACS('02Mt7I33sVeP3hhs8RA98Q22hhOBB3xU');

    if (login && pin) {

        const auth = {
            login: login,
            password: 'Sm^RtSafe_' + pin
        };

        ACS.Users.login(auth, data => {

            if (data.success) {
                if (actionType == 'download') {
                    let files = [];
                    let user = data.users[0];
                    let classname = 'newDiaryEntry';
                    let mainHtml = '';

                    ACS.Objects.query({
                        classname: classname,
                        where: {
                            user_id: user.id
                        },
                        limit: 999,
                        order: 'created_at: Date'
                    }, function(e) {
                        if (e.success) {
                            let objects = e[classname];
                            if (!objects.length) {
                                let message = "no entries found to download";
                                RedirectToIndex(event, 'error', message, callback)
                            } else {
                                tmp.dir({
                                    unsafeCleanup: true
                                }, function(err, path, cleanupCallback) {
                                    if (err) {
                                        throw err;
                                    }
                                    console.log("temp directory path is: ", path);
                                    let zipFile = path + "/smartSafeEntries.zip";
                                    let zipOutput = fs.createWriteStream(zipFile);
                                    let archive = archiver('zip');
                                    archive.pipe(zipOutput);

                                    let counter = 1;

                                    async.each(objects, function(entry, cb) {

                                        let timeZone = 'Australia/Melbourne';
                                        let data = JSON.parse(sjcl.decrypt(getPass(), entry.data));
                                        data.timeZone ? timeZone = data.timeZone : timeZone = timeZone;
                                        let counterString = "00" + counter;
                                        let localPath = moment(entry.created_at).tz(timeZone).format('YYYY-MM-DD-hhmmss') + "_" + counterString.substr(counterString.length - 2);
                                        let subdir = path + "/" + localPath;

                                        fs.mkdirSync(subdir);
                                        counter++;

                                        //	Loop through all attachments...
                                        async.each(data.attachments, (file, fcb) => {

                                            let fileId = file.fileId || file;
                                            ACS.Files.query({
                                                where: {
                                                    id: fileId
                                                }
                                            }, e => {

                                                if (e.success && e.files && e.files.length > 0) {
                                                    let appcFile = e.files[0];
                                                    let parts = appcFile.url.split('.');
                                                    let extension = parts[parts.length - 1];
                                                    let localFileName = appcFile.id + "." + extension;
                                                    let attachmentPath = subdir + "/" + localFileName;
                                                    // Fetch the files to the Directory
                                                    request({
                                                            uri: appcFile.url
                                                        }).pipe(fs.createWriteStream(attachmentPath))
                                                        .on('close', function() {
                                                            console.log('file written: ' + attachmentPath);
                                                            file.localFile = localFileName;
                                                            fcb(); // Continue Loop
                                                        });
                                                } else {
                                                    console.log("unable to find file " + file);
                                                    fcb();
                                                }
                                            });
                                        }, (err) => {

                                            let buffer = '';

                                            buffer += "<html><head><style>\n";
                                            buffer += fs.readFileSync('./src/css/downloadedStyles.css', 'UTF-8').toString();
                                            buffer += "</style></head><body>";
                                            buffer += "<div><a href='../index.html'>&lt; back</a></div>";
                                            buffer += "<h1>Diary Entry</h1><table>\n";
                                            if (data.dateSaved) {
                                                buffer += "<tr><td>Date Saved on Device:</td><td>" + moment(data.dateSaved).tz(timeZone).format('DD MMM YYYY hh:mm:ss') + "</td></tr>\n";
                                            }
                                            buffer += "<tr><td>Date Saved on Server:</td><td>" + moment(entry.created_at).tz(timeZone).format('DD MMM YYYY hh:mm:ss') + " (Server Time)</td></tr>\n";
                                            buffer = writeToBuffer(data, buffer);
                                            buffer += "</table>\n";
                                            buffer += writeAttachments(data, timeZone);
                                            buffer += "<div><a href='../index.html'>&lt; back</a></div>";
                                            buffer += "</body></html>";

                                            fs.writeFileSync(subdir + "/index.html", buffer);
                                            archive.directory(subdir, localPath);
                                            files.push({
                                                date: data.date,
                                                description: data.description,
                                                location: data.location,
                                                ssId: data.ssId,
                                                file: localPath + "/index.html"
                                            });
                                            cb(); // Continue inside asynchronous loop
                                        });
                                    }, err => {

                                        files = _.sortBy(files, entry => {
                                            return entry.ssId ? -1 * entry.ssId : 0;
                                        });

                                        let mainBuffer = '';

                                        mainBuffer += "<html><head><style>\n";
                                        mainBuffer += fs.readFileSync('./src/css/downloadedStyles.css', 'UTF-8');
                                        mainBuffer += "</style></head><body><h1>Archived Diary Entries</h1><table>";

                                        _.each(files, entry => {
                                            mainBuffer += "<tr><td><a href='" + entry.file + "'>" + entry.date + "</a>";
                                            if (entry.description) {
                                                mainBuffer += " - " + entry.description;
                                            }
                                            mainBuffer += "</td></tr>\n";
                                        });
                                        mainBuffer += "</table></body></html>";

                                        fs.writeFileSync(path + "/index.html", mainBuffer);
                                        archive.file(path + "/index.html", {
                                            name: "index.html"
                                        });

                                        // finalize streamming to write zip file
                                        archive.finalize();
                                        zipOutput.on('close', () => {
                                            console.log('stream ending! zipfile path is', zipFile);
                                            console.log('total bytes', archive.pointer());
                                            // write the code to store zip file to S3 bucket and return downloadable link to caller
                                            SaveZipToS3(event, zipFile, callback);
                                        });
                                    });
                                });
                            }
                        } else {
                            // If Query result is fail.
                            RedirectToIndex(event, 'error', 'Query if fail', callback)
                        }
                    });
                } else {
                    // if the user selected send email button
                    console.log('sending email...');
                    let smtpTransport = nodemailer.createTransport({
                        service: "SendGrid",
                        auth: {
                            user: "XXX",
                            pass: "XXXXXXXXX"
                        }
                    });

                    const option = {
                        from: sendTo, // sender address
                        to: sendTo, // comma separated list of receivers
                        subject: "Test email from Amazon Lambda", // Subject line
                        text: "You can download your requested information at:\n\nhttp://plus.smartsafe.org.au/retrieve-evidence?token=" + btoa(login) + "\n"
                    }
                    smtpTransport.sendMail(option, (error, response) => {
                        error ? RedirectToIndex(event, 'error', "Email is not delivered", callback) :
                            RedirectToIndex(event, 'success', 'link sent to ' + sendTo, callback)
                    });
                }

            } else {
                console.log('login failed');
                RedirectToIndex(event, 'error', 'invalid email or pin', callback)
            }
        });
    } else {
        RedirectToIndex(event, 'error', 'Please check your email or pin', callback)
    }
}

const getPass = () => {
    return "Rbs.gbQiJujGp6jVxvAzuNoEeo4ePHMb";
};

const writeToBuffer = (entry, buffer, indent) => {
    var myIndent = indent || "";
    _.each(entry, function(value, key) {
        if (key == 'id' || key == "user_id" || key == "ssId" || key == "localFile" || key == "attachments" || key == "dateSaved" || key == "timeZone") {
            // Do Nothing
        } else if (typeof(value) == 'object') {
            buffer += "<tr><td><table>";
            buffer = writeToBuffer(value, buffer, myIndent + "  ");
            buffer += "</td></tr></table>";
        } else {
            buffer += "<tr>";
            var tempVal = value.toString() == "false" ? "no" : value.toString() == "true" ? "yes" : value;
            buffer += "<td>" + key.replace(/([A-Z])/g, ' $1').replace(/^./, function(str) {
                return str.toUpperCase();
            }) + ":</td><td>" + tempVal + "</td>";
            buffer += "</tr>\n";
        };
    });
    return buffer;
}

const writeAttachments = (data, timeZone) => {
    var buffer = "";
    var attachments = data.attachments;
    if (!attachments) {
        return "";
    }
    _.each(attachments, function(attachment) {
        buffer += "<p>" + attachment.type + " taken from " + attachment.captureType;
        if (attachment.date) {
            buffer += " at " + moment(attachment.date).tz(timeZone).format('DD MMM YYYY h:mma');
        }
        buffer += "<br/>\n";
        if (attachment.type == "audio") {
            buffer += "<audio src='" + attachment.localFile + "' / controls></audio><br/>\n";
            buffer += "or open the file <a href='" + attachment.localFile + "'>" + attachment.localFile + "</a> in your audio player\n"
        } else if (attachment.type == "video") {
            buffer += "<video src='" + attachment.localFile + "' / controls></video><br/>\n";
            buffer += "or open the file <a href='" + attachment.localFile + "'>" + attachment.localFile + "</a> in your video player\n"
        } else if (attachment.type == "photo") {
            buffer += "<img src='" + attachment.localFile + "' />\n";
        } else {
            buffer += "<a href='" + attachment.localFile + "'>Link to File</a>\n";
        }
        buffer += "</p>\n";
    });
    return buffer;
}

const SaveZipToS3 = (event, filepath, callback) => {

    const initOption = {
        signatureVersion: 'v4',
        accessKeyId: 'XXXXXXXXX',
        secretAccessKey: 'XXXXXXXXXXXXXXXXXXX'
    }
    const params = {
        Bucket: "smartsafe-zip",
        Key: 'smartSafeEntries.zip',
        Expires: 5 * 60 // 5 mins
    }

    const s3 = new AWS.S3(initOption);
    const putURL = s3.getSignedUrl('putObject', params) // Actually it doesn't need to sign POST request but implemented
    const getURL = s3.getSignedUrl('getObject', params)

    fs.readFile(filepath, (err, file) => {

        const requestOptions = {
            method: "PUT",
            url: putURL,
            body: file,
            headers: {
                'Content-Type': 'application/zip'
            }
        }
        request(requestOptions, (err, res, body) => {
            if (err) throw err
            RedirectToIndex(event, 'download', getURL, callback)
        })
    })
}

const RedirectToIndex = (event, status, message, callback) => {

    const proto = event.headers["X-Forwarded-Proto"];
    const host = event.headers["Host"];
    const stage = event.requestContext["stage"]
    const indexUrl = proto + "://" + host + "/" + stage + "/index.html";
    const redirectUrl = indexUrl + "?status=" + status + "&message=" + message

    let response = {
        statusCode: 301,
        headers: {
            "Location": redirectUrl
        }
    };
    callback(null, response);
}
