const AWS = require("aws-sdk"); // from AWS SDK
const fs = require("fs"); // from node.js
const path = require("path"); // from node.js

const prefix = process.argv[2]  || false;
const dirname = process.argv[3] || false;
const bprefix = process.argv[4] || false;
const bucket = process.argv[5]  || false;

if(prefix !== '-d' || !prefix || !dirname ) {
    console.log('\n\n * Please check your command. must provide directory path you want to upload.\n\n\n');
    process.exit();
}
if(bprefix !== "-b" || !bprefix || !bucket) {
    console.log('\n\n * Please provide S3 bucket name. \n\n\n');
    process.exit();
}


const config = {
  s3BucketName: bucket,
  folderPath: dirname 
};

// initialize S3 client
const s3 = new AWS.S3({ signatureVersion: 'v4' });

// resolve full folder path
const distFolderPath = path.join(__dirname, config.folderPath);

// get of list of files from 'dist' directory
fs.readdir(distFolderPath, (err, files) => {

  if(!files || files.length === 0) {
    console.log(`provided folder '${distFolderPath}' is empty or does not exist.`);
    console.log('Make sure your project was compiled!');
    return;
  }

  // for each file in the directory
  for (const fileName of files) {

    // get the full path of the file
    const filePath = path.join(distFolderPath, fileName);
    
    // ignore if directory
    if (fs.lstatSync(filePath).isDirectory()) {
      continue;
    }

    // read file contents
    fs.readFile(filePath, (error, fileContent) => {
      // if unable to read file contents, throw exception
      if (error) { throw error; }

      // upload file to S3
      s3.putObject({
        Bucket: config.s3BucketName,
        Key: fileName,
        Body: fileContent,
        ACL:'public-read'
      }, (err, res) => {
          if(err) {
              console.log(err.message)
          } else {
            console.log(`\n - Successfully uploaded '${fileName}'!`);
          }
      });
    });
  }
});