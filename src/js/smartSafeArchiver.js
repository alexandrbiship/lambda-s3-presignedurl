var SmartSafeArchiver = {};

//SmartSafeArchiver.server = "https://aba5b9ef90af479c0b2a0361f81280e8801d383f.cloudapp-enterprise.appcelerator.com";
SmartSafeArchiver.server = "https://2u8wlv2yx2.execute-api.ap-southeast-2.amazonaws.com/dev";

SmartSafeArchiver.currentPin = "";
SmartSafeArchiver.currentLogin = "";
SmartSafeArchiver.sendTo = "";
SmartSafeArchiver.autoSubmit = false;

SmartSafeArchiver.numberClick = function(num) {
	if (SmartSafeArchiver.currentPin.length < 4) {
		SmartSafeArchiver.currentPin += num.toString();
	}
	SmartSafeArchiver.redraw();
	if (SmartSafeArchiver.currentPin.length == 4) {
		SmartSafeArchiver.validateScreen1();
	}
};

SmartSafeArchiver.validateScreen1 = function() {
	SmartSafeArchiver.currentLogin = document.getElementById('ssLogin').value.trim();
	var errorMessage = "";
	if (SmartSafeArchiver.currentLogin == "") {
		errorMessage += "- You must enter an email address\n";
	}
	if (SmartSafeArchiver.currentPin.length != 4) {
		errorMessage += "- PIN must be 4 digits\n";
	}
	if (errorMessage) {
		alert(errorMessage);
		return;
	}
	SmartSafeArchiver.doScreen2();
};

SmartSafeArchiver.doClear = function(num) {
	SmartSafeArchiver.currentPin = "";
	SmartSafeArchiver.redraw();
};

SmartSafeArchiver.doDelete = function(num) {
	if (SmartSafeArchiver.currentPin.length > 0) {
		SmartSafeArchiver.currentPin = SmartSafeArchiver.currentPin.substr(0, SmartSafeArchiver.currentPin.length - 1);
		SmartSafeArchiver.redraw();
	}
};

SmartSafeArchiver.redraw = function() {
	for (var i = 1; i <= 4; i++) {
		var pinDot = document.getElementById('pinDot' + i);
		pinDot.className = "pinDot" + (i <= SmartSafeArchiver.currentPin.length ? " active" : "");
	}
};

SmartSafeArchiver.showError = function(message) {
	document.getElementById('ssErrorHolder').innerHTML = message;
};

SmartSafeArchiver.clearError = function() {
	SmartSafeArchiver.showError("");
};

SmartSafeArchiver.doPOST = function() {
	var theForm = document.forms['smartsafeArchiverForm'];
	theForm.submit();
	SmartSafeArchiver.doScreen3();
};

SmartSafeArchiver.getScreen1 = function() {
	var screen1 = "<div class='ssHolder'><form>";
	if (SmartSafeArchiver.autoSubmit) {
		screen1 += "<div style='display:none;'>";
	} else {
		screen1 += "<div>";
		screen1 += "<p>This will allow you to email your diary entries to your chosen email address. To get started, enter your email and PIN below.</p>";
	}
	screen1 += "<input id='ssLogin' name='login' type='email' value='" + SmartSafeArchiver.currentLogin + "' placeholder='email'>";
	screen1 += "</div>";
	screen1 += "<div id='pin'>";
	screen1 += "<p>enter your pin</p>";

	// draw the buttons
	screen1 += "<div id='pinDots'>";

	for (var i = 1; i <= 4; i++) {
		screen1 += "<div class='pinDotHolder'>";
		screen1 += "<div id='pinDot" + i + "' class='pinDot'></div>";
		screen1 += "</div>";
	}
	screen1 += "</div>";
	screen1 += "<div style='clear:both'>";

	for (var counter = 1; counter <= 9; counter++) {
		screen1 += "<div class='ssPinButtonHolder'>";
		screen1 += "<input type='button' class='ssPinButton ssNumberButton' value='" + counter + "' onClick='SmartSafeArchiver.numberClick(" + counter + ")'>";
		screen1 += "</div>";
	}
	screen1 += "<div class='ssPinButtonHolder'><input type='button' class='ssPinButton' value='clear' onClick='SmartSafeArchiver.doClear()'></div>";
	screen1 += "<div class='ssPinButtonHolder'><input type='button' class='ssPinButton ssNumberButton' value='0' onClick='SmartSafeArchiver.numberClick(0)'></div>";
	screen1 += "<div class='ssPinButtonHolder'><input type='button' class='ssPinButton' value='delete' onClick='SmartSafeArchiver.doDelete()'></div>";
	screen1 += "</div>";
	screen1 += "</div>";
	screen1 += "<div style='clear:both'>";
	screen1 += "<input type='button' class='ssNextButton' onClick='SmartSafeArchiver.validateScreen1();' value='NEXT'>";
	screen1 += "</div>";
	screen1 += "</form></div>";
	return screen1;

};

SmartSafeArchiver.doScreen1 = function() {
	var div = document.getElementById('archiveLogin');
	div.innerHTML = SmartSafeArchiver.getScreen1();
};

SmartSafeArchiver.resetPIN = function() {
	SmartSafeArchiver.doScreen1();
	SmartSafeArchiver.doClear();
};

SmartSafeArchiver.changeActionType = function(val) {
	if (val == 'download') {
		document.getElementById('ssEmail').style.display = 'none';
		document.getElementById('ssSubmitButton').value = 'DOWNLOAD';
	} else {
		document.getElementById('ssEmail').style.display = 'block';
		document.getElementById('ssSubmitButton').value = 'SEND LINK';
	}
};

SmartSafeArchiver.getScreen2 = function() {
	var screen2 = "<div class='ssHolder'><form action='" + SmartSafeArchiver.server + "/archive' method='POST' name='smartsafeArchiverForm'>";

	screen2 += "<input type='hidden' name='login' value='" + SmartSafeArchiver.currentLogin + "'>";
	screen2 += "<input type='hidden' name='pin' value='" + SmartSafeArchiver.currentPin + "'>";
	screen2 += "<div id='ssErrorHolder'></div>";
	screen2 += "<div style='text-align:left;'>";
	screen2 += "<input type='radio' name='actionType' value='download' onClick='SmartSafeArchiver.changeActionType(this.value);' checked='checked'> download evidence</p>";
	screen2 += "<input type='radio' name='actionType' value='email' onClick='SmartSafeArchiver.changeActionType(this.value);'> email me a link to download my evidence</p>";
	screen2 += "</div>";
	screen2 += "<div id='ssEmail' style='display:none;'>";
	screen2 += "<p>Please enter the email address you would like to send the archive to:</p>";
	screen2 += "<input id='ssSendTo' name='sendTo' type='email' value='" + SmartSafeArchiver.currentLogin + "' placeholder='email'>";
	screen2 += "</div>";
	screen2 += "<div style='margin-top:10px;'><input id='ssSubmitButton' type='button' class='ssNextButton' onClick='SmartSafeArchiver.doPOST();' value='SEND ARCHIVE'></div>";
	screen2 += "<div style='margin-top:40px;'><input type='button' class='ssNextButton' onClick='SmartSafeArchiver.resetPIN();' value='BACK'></div>";
	screen2 += "</form></div>";
	return screen2;
};

SmartSafeArchiver.doScreen2 = function() {
	var div = document.getElementById('archiveLogin');
	div.innerHTML = SmartSafeArchiver.getScreen2();
	SmartSafeArchiver.changeActionType('download');
	if (SmartSafeArchiver.autoSubmit) {
		SmartSafeArchiver.doPOST();
	}
};

SmartSafeArchiver.doScreen3 = function() {
	var div = document.getElementById('archiveLogin');
	div.innerHTML = "<div class='ssHolder'>Please wait while we prepare your file....<div style='margin-top:50px;'><img src='" + "https://s3-ap-southeast-2.amazonaws.com/smartsafe-assets/spiffygif_70x70.gif'/></div><div>";
	setTimeout(function() {
		SmartSafeArchiver.doScreen4('Thank you! You will be prompted to download your file shortly!')
	}, 5000);
};

SmartSafeArchiver.doScreen4 = function(message) {
	var div = document.getElementById('archiveLogin');
	div.innerHTML = "<div class='ssHolder'>" + message + "</div><div><a href='#' onclick='history.back(-1);'>Back</a></div>";
};

SmartSafeArchiver.getQueryVariable = function(variable) {
	var query = window.location.search.substring(1);
	var vars = query.split('&');
	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split('=');
		if (decodeURIComponent(pair[0]) == variable) {
			return decodeURIComponent(pair[1]);
		}
	}
	console.log('Query variable %s not found', variable);
};

(function(w, d) {
	d.addEventListener('DOMContentLoaded', function(e) {
		var message = SmartSafeArchiver.getQueryVariable('message');
		var status = SmartSafeArchiver.getQueryVariable('status');
		var token = SmartSafeArchiver.getQueryVariable('token');
		if (status && message) {
			if (status == "success") {
				SmartSafeArchiver.doScreen4("Thank You! " + message);
			} else if(status == "download") {
				var url= window.location.href.split("status=download&message=");
				var downloadURL = url[1];
				SmartSafeArchiver.doScreen4("<h3>Thank you for downloading.</h3><br> <p>If you are getting trouble, click this link directly</p><a href="+downloadURL+">"+downloadURL+"</a>");
				var win = window.open(downloadURL, "_blank");
				setTimeout(function() {
					win.close();
				}, 5000);
			} else {
				SmartSafeArchiver.doScreen4(message);
			}
		} else {
			if (token) {
				SmartSafeArchiver.currentLogin = atob(token);
				SmartSafeArchiver.autoSubmit = true;
			}
			SmartSafeArchiver.doScreen1();
		}
	});
})(window, document);
